package command

import (
	"bytes"
	"errors"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"
)

var (
	lastExitCode = 0
	fakeOsExiter = func(rc int) {
		lastExitCode = rc
	}
	fakeErrWriter = &bytes.Buffer{}
)

func init() {
	cli.OsExiter = fakeOsExiter
	cli.ErrWriter = fakeErrWriter
}

func TestSearch(t *testing.T) {
	t.Run("found", func(t *testing.T) {
		var tcs = []struct {
			Filename   string
			WantOutput string
		}{
			{
				Filename:   "pom.xml",
				WantOutput: "fixtures/search",
			},
			{
				Filename:   "main.c",
				WantOutput: "fixtures/search/c/c",
			},
			{
				Filename:   "c",
				WantOutput: "fixtures/search/c",
			},
		}

		for _, tc := range tcs {
			t.Run(tc.Filename, func(t *testing.T) {
				match := func(path string, info os.FileInfo) (bool, error) {
					return info.Name() == tc.Filename, nil
				}
				buf := new(bytes.Buffer)
				log.SetOutput(buf)
				app := cli.NewApp()
				app.Name = "search"
				app.Usage = "Test search command"
				app.Commands = []*cli.Command{Search(Config{Match: match})}
				err := app.Run([]string{"analyzer", "search", "fixtures/search"})
				require.NoError(t, err)
				got := strings.TrimSpace(buf.String())

				require.Contains(t, got, tc.WantOutput)
			})
		}
	})

	t.Run("match error", func(t *testing.T) {
		matchErr := errors.New("Error in match function")
		match := func(path string, info os.FileInfo) (bool, error) {
			return false, matchErr
		}
		app := cli.NewApp()
		app.Name = "search"
		app.Usage = "Test search command"
		app.Commands = []*cli.Command{Search(Config{Match: match})}
		err := app.Run([]string{"analyzer", "search", "fixtures/search"})
		require.EqualError(t, err, matchErr.Error())
	})

	t.Run("exit code should be 3: not found", func(t *testing.T) {
		match := func(path string, info os.FileInfo) (bool, error) {
			return false, nil
		}
		app := cli.NewApp()
		app.Name = "search"
		app.Usage = "Test search command"
		app.Commands = []*cli.Command{Search(Config{Match: match})}
		app.Run([]string{"analyzer", "search", "fixtures/search"})
		want, got := 3, lastExitCode
		require.Equal(t, want, got)
	})

	t.Run("exit code should be 2: invalid args", func(t *testing.T) {
		match := func(path string, info os.FileInfo) (bool, error) {
			return false, nil
		}
		app := cli.NewApp()
		app.Name = "search"
		app.Usage = "Test search command"
		app.Commands = []*cli.Command{Search(Config{Match: match})}
		app.Writer = ioutil.Discard
		app.Run([]string{"analyzer", "search"})
		want, got := 2, lastExitCode
		require.Equal(t, want, got)
	})
}
