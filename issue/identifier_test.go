package issue

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIdentifier(t *testing.T) {
	var tcs = []struct {
		name  string
		parse string // argument given to parseIdentifier
		got   Identifier
		want  Identifier
	}{
		{
			name:  "CVEIdentifier",
			parse: "CVE-123",
			got:   CVEIdentifier("CVE-123"),
			want: Identifier{
				Type:  "cve",
				Name:  "CVE-123",
				Value: "CVE-123",
				URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-123",
			},
		},
		{
			name:  "CWEIdentifier",
			parse: "CWE-123",
			got:   CWEIdentifier(123),
			want: Identifier{
				Type:  "cwe",
				Name:  "CWE-123",
				Value: "123",
				URL:   "https://cwe.mitre.org/data/definitions/123.html",
			},
		},
		{
			name:  "OSVDBIdentifier",
			parse: "OSVDB-123",
			got:   OSVDBIdentifier("OSVDB-123"),
			want: Identifier{
				Type:  "osvdb",
				Name:  "OSVDB-123",
				Value: "OSVDB-123",
				URL:   "https://cve.mitre.org/data/refs/refmap/source-OSVDB.html",
			},
		},
		{
			name:  "USNIdentifier",
			parse: "USN-123",
			got:   USNIdentifier("USN-123"),
			want: Identifier{
				Type:  "usn",
				Name:  "USN-123",
				Value: "USN-123",
				URL:   "https://usn.ubuntu.com/123/",
			},
		},
		{
			name:  "WASCIdentifier",
			parse: "WASC-02",
			got:   WASCIdentifier(2),
			want: Identifier{
				Type:  "wasc",
				Name:  "WASC-02",
				Value: "2",
				URL:   "http://projects.webappsec.org/Insufficient-Authorization",
			},
		},
		{
			name:  "RHSAIdentifier",
			parse: "RHSA-2019:3892",
			got:   RHSAIdentifier("RHSA-2019:3892"),
			want: Identifier{
				Type:  "rhsa",
				Name:  "RHSA-2019:3892",
				Value: "RHSA-2019:3892",
				URL:   "https://access.redhat.com/errata/RHSA-2019:3892",
			},
		},
		{
			name:  "ELSAIdentifier",
			parse: "ELSA-2017-1101",
			got:   ELSAIdentifier("ELSA-2017-1101"),
			want: Identifier{
				Type:  "elsa",
				Name:  "ELSA-2017-1101",
				Value: "ELSA-2017-1101",
				URL:   "https://linux.oracle.com/errata/ELSA-2017-1101.html",
			},
		},
		{
			name:  "H1Identifier",
			parse: "HACKERONE-350401",
			got:   H1Identifier("HACKERONE-350401"),
			want: Identifier{
				Type:  "hackerone",
				Name:  "HACKERONE-350401",
				Value: "350401",
				URL:   "https://hackerone.com/reports/350401",
			},
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			require.Equal(t, tc.want, tc.got)
		})
	}

	t.Run("ParseIdentifierID", func(t *testing.T) {
		for _, tc := range tcs {
			got, ok := ParseIdentifierID(tc.parse)
			require.Truef(t, ok, "Expected %s to be parsed successfully", tc.parse)
			require.Equal(t, tc.want, got)
		}
	})
}
