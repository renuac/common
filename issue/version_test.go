package issue

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCurrentVersion(t *testing.T) {
	want := Version{Major: 3, Minor: 0, Patch: 0, PreRelease: ""}
	got := CurrentVersion()
	require.Equal(t, want, got)
}

func TestVersion(t *testing.T) {
	t.Run("String", func(t *testing.T) {
		want := "12.34.56"
		got := Version{Major: 12, Minor: 34, Patch: 56}.String()
		require.Equal(t, want, got)
	})

	t.Run("String with PreRelease", func(t *testing.T) {
		want := "12.34.56-rc1"
		got := Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"}.String()
		require.Equal(t, want, got)
	})

	t.Run("MarshalJSON", func(t *testing.T) {
		want := []byte(`"12.34.56"`)
		got, err := Version{Major: 12, Minor: 34, Patch: 56}.MarshalJSON()
		require.NoError(t, err)
		require.Equal(t, want, got)
	})

	t.Run("MarshalJSON with PreRelease", func(t *testing.T) {
		want := []byte(`"12.34.56-rc1"`)
		got, err := Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"}.MarshalJSON()
		require.NoError(t, err)
		require.Equal(t, string(want), string(got))
	})

	t.Run("UnarshalJSON", func(t *testing.T) {
		var tcs = []struct {
			name  string
			input string
			want  Version
		}{
			{
				name:  "Empty",
				input: `null`,
				want:  Version{Major: VersionMajor, Minor: VersionMinor},
			},
			{
				name:  "Major",
				input: `"12"`,
				want:  Version{Major: 12},
			},
			{
				name:  "Major.Minor",
				input: `"12.34"`,
				want:  Version{Major: 12, Minor: 34},
			},
			{
				name:  "Major.Minor.Patch",
				input: `"12.34.56"`,
				want:  Version{Major: 12, Minor: 34, Patch: 56},
			},
			{
				name:  "Major.Minor.Patch-Pre",
				input: `"12.34.56-rc1"`,
				want:  Version{Major: 12, Minor: 34, Patch: 56, PreRelease: "rc1"},
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				var got Version
				err := got.UnmarshalJSON([]byte(tc.input))
				require.NoError(t, err)
				require.Equal(t, tc.want, got)
			})
		}
	})
}
