package pathfilter

import "testing"

func TestFilter(t *testing.T) {
	var tcs = []struct {
		ExcludedPaths []string
		WantExcluded  []string
		WantIncluded  []string
	}{
		{
			ExcludedPaths: []string{"pom.*"},
			WantExcluded: []string{

				"app/test/pom.xml",
				"test/pom.xml",
				"xyz/pom.xml",
				"abc/def/pom.apple",
			},
			WantIncluded: []string{
				"apple.pom",
			},
		},
		{
			ExcludedPaths: []string{
				"banana.xml",
				"test",
				"/xyz",
				"abc/def",
			},
			WantExcluded: []string{

				"app/test/pom.xml",
				"banana.xml",
				"app/banana.xml",
				"test/pom.xml",
				"xyz/pom.xml",
				"abc/def/pom.xml",
			},
			WantIncluded: []string{
				"app/pom.xml",
				"app/xyz/pom.xml",
			},
		},
	}

	for _, tc := range tcs {
		filter := Filter{ExcludedPaths: tc.ExcludedPaths}
		t.Run(filter.String(), func(t *testing.T) {
			for _, path := range tc.WantIncluded {
				if filter.IsExcluded(path) {
					t.Errorf("expected path to be included: %s", path)
					continue
				}
			}

			for _, path := range tc.WantExcluded {
				if !filter.IsExcluded(path) {
					t.Errorf("expected path to be excluded: %s", path)
					continue
				}
			}
		})
	}
}
